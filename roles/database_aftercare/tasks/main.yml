---

- name: when database_name is not ''
  block:

  - name: create systemv start/stop script for the oracle database to autostart
    block:

    - name: create systemv start/stop script for oracle database autostart
      shell: |
        echo "#!/bin/bash
              #
              # chkconfig: 35 99 10   
              # description: Starts and stops the oracle database
              #
              # Set ORA_HOME to be equivalent to the $ORACLE_HOME
              # from which you wish to execute dbstart and dbshut;
              #
              # Set ORA_OWNER to the user id of the owner of the
              # Oracle database in ORA_HOME.
              # MOS note 222813.1

              ORA_HOME={{ oracle_home }}
              ORA_OWNER=oracle

              case \"\$1\" in
                'start')
                  # Start the Oracle databases:
                  # The following command assumes that the oracle login
                  # will not prompt the user for any values
                  su - \$ORA_OWNER -c \"\$ORA_HOME/bin/dbstart \$ORA_HOME\"

                  touch /var/lock/subsys/oracle_database
                ;;

                'stop')
                  # Stop the Oracle databases:
                  # The following command assumes that the oracle login
                  # will not prompt the user for any values
                  su - \$ORA_OWNER -c \"\$ORA_HOME/bin/dbshut \$ORA_HOME\"

                  rm -f /var/lock/subsys/oracle_database
                ;;
              esac

              # End of script oracle_database" > /etc/init.d/oracle_database
        chmod 755 /etc/init.d/oracle_database
      args:
        creates: /etc/init.d/oracle_database
      become_user: root

    - name: enable oracle_database service at startup
      service:
        name: oracle_database
        enabled: yes
      become_user: root

    when: ( ansible_service_mgr == 'upstart' or ansible_service_mgr == 'sysvinit' ) and asm_version == ''

  - name: create autostart unit file for systemd when asm is not installed
    block:

    - name: create systemd unit file for oracle database autostart
      shell: |
        echo "[Unit]
              Description=Oracle database
              Requires=network-online.target
              After=network.target network-online.target multi-user.target
  
              [Service]
              Type=forking
              RemainAfterExit=yes
              KillMode=none
              TimeoutStopSec=5min

              User=oracle
              Group=oinstall
              ExecStart={{ oracle_home }}/bin/dbstart {{ oracle_home }}
              ExecStop={{ oracle_home}}/bin/dbshut {{ oracle_home }}
              Restart=no

              [Install]
              WantedBy=default.target" > /etc/systemd/system/oracle_database.service
      args:
        creates: /etc/systemd/system/oracle_database.service
      become_user: root

    - name: set the oracle database service to be enabled at boot
      systemd:
        name: oracle_database
        enabled: yes
        daemon_reload: yes
      become_user: root
        
    when: ansible_service_mgr == 'systemd' and asm_version == ''

  - name: test if the listener is running
    shell: >
      {{ oracle_home }}/bin/lsnrctl stat 
    environment:
      ORACLE_HOME: "{{ oracle_home }}"
      ORACLE_SID: "{{ database_name }}"
    changed_when: listener_running_test.rc > 0
    register: listener_running_test
    failed_when: listener_running_test.rc > 255
  
  - name: start the listener and register database if the listener is not running
    shell: |
      {{ oracle_home }}/bin/lsnrctl start
      echo "alter system register;" | {{ oracle_home }}/bin/sqlplus / as sysdba
    environment:
      ORACLE_HOME: "{{ oracle_home }}"
      ORACLE_SID: "{{ database_name }}"
    when: listener_running_test.changed

  - name: query if password expiration is set to unlimited in main db
    shell: |
      echo "select limit from dba_profiles where profile = 'DEFAULT' and resource_name = 'PASSWORD_LIFE_TIME';
           " | {{ oracle_home }}/bin/sqlplus / as sysdba
    environment:
      ORACLE_HOME: "{{ oracle_home }}"
      ORACLE_SID: "{{ database_name }}"
    register: main_db_password_expiration
    changed_when: "'UNLIMITED' not in main_db_password_expiration.stdout"
    notify: set main db password expiration to unlimited

  - name: handling specific to pluggable database
    block:

    - name: query if {{ database_name | upper }}_PDB is open
      shell: |
        echo "select open_mode from v\$pdbs where name = '{{ database_name | upper }}_PDB';
             " | {{ oracle_home }}/bin/sqlplus / as sysdba
      environment:
        ORACLE_HOME: "{{ oracle_home }}"
        ORACLE_SID: "{{ database_name }}"
      register: pdb_open
    
    - name: open pdb {{ database_name | upper }}_PDB if it wasn't open (reports MOUNTED)
      shell: |
        echo "alter pluggable database {{ database_name }}_pdb open;
             " | {{ oracle_home }}/bin/sqlplus / as sysdba
      environment:
        ORACLE_HOME: "{{ oracle_home }}"
        ORACLE_SID: "{{ database_name }}"
      when: "'MOUNTED' in pdb_open.stdout" 

    - name: save state of opened pluggable database 
      shell: |
        echo "alter pluggable database {{ database_name }}_pdb save state;
             " | {{ oracle_home }}/bin/sqlplus / as sysdba
      environment:
        ORACLE_HOME: "{{ oracle_home }}"
        ORACLE_SID: "{{ database_name }}"

    - name: query if password expiration is set to unlimited in pdb
      shell: |
        echo "select limit from dba_profiles where profile = 'DEFAULT' and resource_name = 'PASSWORD_LIFE_TIME';
             " | {{ oracle_home }}/bin/sqlplus -S sys/{{ global_password }}@//localhost/{{ database_name}}_pdb as sysdba
      environment:
        ORACLE_HOME: "{{ oracle_home }}"
        ORACLE_SID: "{{ database_name }}"
      register: pdb_password_expiration
      changed_when: "'UNLIMITED' not in pdb_password_expiration.stdout"
      notify: set pdb password expiration to unlimited

    when: pluggable_database == "Y"
  
  when: database_name != ''
